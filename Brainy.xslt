﻿<!--
This stylesheet transforms a brainfuck (http://en.wikipedia.org/wiki/Brainfuck) xml document to an output brainfuck xml document, by running the Brainfuck program in the /brainfuck/code element with /brainfuck/input as input. Be sure to escape the brainfuck statements properly as XML requires. Characters in the code element which are not brainfuck instructions are ignored. No xslt extensions are used, just plain xslt 1.0. The array is dynamically resized on the right (>) side when needed and can be considered to be of infinite size. To see the array for every statement processed, pass parameter "debug" with a value evaluating to true() to the stylesheet. Be aware that, since this stylesheet works with recursion, some xslt processors will eat lots of memory and/or segfault when running large complicated Brainfuck programs. For example, the documents: <?xml version="1.0" encoding="iso-8859-1"?> <brainfuck> <code>,[.,]</code> <input>Hello World!</input> </brainfuck> and: (The extra spaces are just to make this a valid XML comment.) <?xml version="1.0" encoding="iso-8859-1"?> <brainfuck> <code><![CDATA[ ++++++++++[>+++++++>++++++++++>+++>+<<<<-] >++.>+.+++++++..+++.>++.<<+++++++++++++++. >.+++.- - - - - -.- - - - - - - -.>+. ]]></code> </brainfuck> both produce the document: <?xml version="1.0" encoding="iso-8859-1"?> <brainfuck> <result>Hello World!</result> </brainfuck> The document: <?xml version="1.0" encoding="iso-8859-1"?> <brainfuck> <code><![CDATA[ ,>,,>++++++++[<- - - - - -<- - - - - ->>-] <<[>[>+>+<<-]>>[<<+>>-]<<<-] >>>++++++[<++++++++>-],<.>. ]]></code> <input>2*3</input> </brainfuck> will produce: <?xml version="1.0" encoding="iso-8859-1"?> <brainfuck> <result>6</result> </brainfuck> This stylesheet is available at http://www.lysator.liu.se/~jc/hacks/brainfuck.xslt
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" version="1.0" encoding="iso-8859-1" omit-xml-declaration="no" indent="yes"/>
<xsl:param name="debug" select="false()"/>
<xsl:template match="/brainfuck">
<xsl:variable name="code">
<xsl:call-template name="preprocess">
<xsl:with-param name="code" select="normalize-space(code)"/>
</xsl:call-template>
</xsl:variable>
<xsl:if test="$debug">
<xsl:message>
Code:
<xsl:value-of select="$code"/>
</xsl:message>
</xsl:if>
<brainfuck>
<result>
<xsl:call-template name="process">
<xsl:with-param name="code" select="$code"/>
<xsl:with-param name="input" select="input"/>
</xsl:call-template>
</result>
</brainfuck>
</xsl:template>
<!-- Remove junk characters and calculate loop lengths -->
<xsl:template name="preprocess">
<xsl:param name="code"/>
<xsl:variable name="statement" select="substring($code, 1, 1)"/>
<xsl:if test="contains("<>+-.,", $statement)">
<xsl:value-of select="$statement"/>
</xsl:if>
<xsl:if test="$statement = "["">
<xsl:variable name="looplen">
<xsl:call-template name="getlooplength">
<xsl:with-param name="code" select="substring($code, 2)"/>
</xsl:call-template>
</xsl:variable>
<xsl:variable name="loop">
<xsl:call-template name="preprocess">
<xsl:with-param name="code" select="substring($code, 2, $looplen)"/>
</xsl:call-template>
</xsl:variable>
<xsl:value-of select="concat('[', string-length($loop), ' ', $loop, ']', string-length($loop), ' ')"/>
<xsl:call-template name="preprocess">
<xsl:with-param name="code" select="substring($code, $looplen+3)"/>
</xsl:call-template>
</xsl:if>
<xsl:if test="$statement and $statement != '['">
<xsl:call-template name="preprocess">
<xsl:with-param name="code" select="substring($code, 2)"/>
</xsl:call-template>
</xsl:if>
</xsl:template>
<!-- Find the matching ] -->
<xsl:template name="getlooplength">
<xsl:param name="code"/>
<xsl:variable name="loopcandidate" select="substring-before($code, "]")"/>
<xsl:choose>
<xsl:when test="contains($loopcandidate, "[")">
<xsl:variable name="beforelen" select="string-length(substring-before($code, "["))"/>
<xsl:variable name="nextlen">
<xsl:call-template name="getlooplength">
<xsl:with-param name="code" select="substring-after($code, "[")"/>
</xsl:call-template>
</xsl:variable>
<xsl:variable name="afterlen">
<xsl:call-template name="getlooplength">
<xsl:with-param name="code" select="substring($code, 1+$beforelen+1+$nextlen+1)"/>
</xsl:call-template>
</xsl:variable>
<xsl:value-of select="$beforelen+1+$nextlen+1+$afterlen"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="string-length($loopcandidate)"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>
<!-- Run the code -->
<xsl:template name="process">
<xsl:param name="code"/>
<xsl:param name="input"/>
<xsl:param name="array" select="" 000""/>
<xsl:param name="arraypos" select="0"/>
<xsl:param name="codepos" select="1"/>
<xsl:variable name="value" select="number(substring($array, $arraypos+2, 3))"/>
<xsl:variable name="statement" select="substring($code, $codepos, 1)"/>
<xsl:if test="$debug">
<xsl:message>
<xsl:choose>
<xsl:when test="$statement">
<xsl:value-of select="concat("Processing: ", $statement, " ", $array, " :", $arraypos div 4)"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="concat("End: ", $array, " :", $arraypos div 4)"/>
</xsl:otherwise>
</xsl:choose>
</xsl:message>
</xsl:if>
<!-- Increase or decrease pointer -->
<xsl:if test="($statement = ">") or ($statement = "<")">
<xsl:variable name="newarraypos" select="$arraypos+4 - 8*($statement="<")"/>
<xsl:variable name="newarray">
<xsl:choose>
<xsl:when test="$newarraypos >= string-length($array)">
<xsl:value-of select="concat($array, " 000")"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="$array"/>
</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:call-template name="process">
<xsl:with-param name="code" select="$code"/>
<xsl:with-param name="codepos" select="$codepos+1"/>
<xsl:with-param name="input" select="$input"/>
<xsl:with-param name="array" select="$newarray"/>
<xsl:with-param name="arraypos" select="$newarraypos"/>
</xsl:call-template>
</xsl:if>
<!-- Increase or decrease byte at pointer -->
<xsl:if test="($statement = "+") or ($statement = "-")">
<xsl:variable name="newvalue">
<xsl:choose>
<xsl:when test="$statement = "+"">
<xsl:value-of select="($value + 1) mod 256"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="($value + 255) mod 256"/>
</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:variable name="newstr">
<xsl:choose>
<xsl:when test="$newvalue != 0">
<xsl:number format=" 001" value="$newvalue"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="" 000""/>
</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:call-template name="process">
<xsl:with-param name="code" select="$code"/>
<xsl:with-param name="codepos" select="$codepos+1"/>
<xsl:with-param name="input" select="$input"/>
<xsl:with-param name="array" select="concat(substring($array, 1, $arraypos), $newstr, substring($array, $arraypos+5))"/>
<xsl:with-param name="arraypos" select="$arraypos"/>
</xsl:call-template>
</xsl:if>
<!--
Output from or input to the byte at the pointer (ASCII).
-->
<xsl:if test="($statement = ".") or ($statement = ",")">
<xsl:variable name="ascii0" select=""  ""/>
<xsl:variable name="ascii1">!"#$%&'()*+,-./0123456789:;<=>?</xsl:variable>
<xsl:variable name="ascii2" select=""@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_""/>
<xsl:variable name="ascii3" select=""`abcdefghijklmnopqrstuvwxyz{|}~""/>
<!-- Fill out with the rest of the 128-255 characters -->
<xsl:variable name="ascii4" select="" ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþ""/>
<xsl:variable name="ascii" select="concat($ascii0, $ascii1, $ascii2, $ascii3, $ascii4)"/>
<xsl:choose>
<xsl:when test="$statement = "."">
<xsl:if test="$value != 0">
<xsl:value-of select="substring($ascii, $value, 1)"/>
</xsl:if>
<xsl:call-template name="process">
<xsl:with-param name="code" select="$code"/>
<xsl:with-param name="codepos" select="$codepos+1"/>
<xsl:with-param name="input" select="$input"/>
<xsl:with-param name="array" select="$array"/>
<xsl:with-param name="arraypos" select="$arraypos"/>
</xsl:call-template>
</xsl:when>
<xsl:otherwise>
<xsl:variable name="newvalue">
<xsl:if test="substring($input, 1, 1)=" "">
<xsl:value-of select="" 032""/>
</xsl:if>
<xsl:if test="substring($input, 1, 1)=""">
<xsl:value-of select="" 000""/>
</xsl:if>
<xsl:if test="substring($input, 1, 1)!="" and substring($input, 1, 1)!=" "">
<xsl:number format=" 001" value="string-length(substring-before($ascii, substring($input, 1, 1)))+1"/>
</xsl:if>
</xsl:variable>
<xsl:call-template name="process">
<xsl:with-param name="code" select="$code"/>
<xsl:with-param name="codepos" select="$codepos+1"/>
<xsl:with-param name="input" select="substring($input, 2)"/>
<xsl:with-param name="array" select="concat(substring($array, 1, $arraypos), $newvalue, substring($array, $arraypos+5))"/>
<xsl:with-param name="arraypos" select="$arraypos"/>
</xsl:call-template>
</xsl:otherwise>
</xsl:choose>
</xsl:if>
<!-- Loop -->
<xsl:if test="($statement = "[" or $statement = "]")">
<xsl:variable name="looplen" select="substring-before(substring($code, $codepos+1), " ")"/>
<xsl:variable name="offset">
<xsl:choose>
<xsl:when test="( $value and $statement = "[") or (not($value) and $statement = "]")">
<xsl:value-of select="string-length($looplen)+2"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="(string-length($looplen)+2)*2*($statement="[") + number($looplen)*(($statement="[")*2-1)"/>
</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:call-template name="process">
<xsl:with-param name="code" select="$code"/>
<xsl:with-param name="codepos" select="$codepos + $offset"/>
<xsl:with-param name="input" select="$input"/>
<xsl:with-param name="array" select="$array"/>
<xsl:with-param name="arraypos" select="$arraypos"/>
</xsl:call-template>
</xsl:if>
</xsl:template>
</xsl:stylesheet>